package ch.cern.cms.ecal.radiatest.ds;

import ch.cern.cms.ecal.radiatest.domain.DpName2Id;
import ch.cern.cms.ecal.radiatest.domain.RadSensorDpt;
import ch.cern.cms.ecal.radiatest.domain.SensorReadouts;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;


public interface DpeDao extends CrudRepository<DpName2Id, Long>
{
    @Query("select c from DpName2Id c where c.dpName in :dpName")
    List<DpName2Id> retrieveDpNames(@Param("dpName")String [] dpNames);

    @Query("select r from RadSensorDpt r where r.dPid in :dPid and r.changeDate BETWEEN :startDate AND :endDate")
    List<RadSensorDpt> retrieveReadouts2(@Param("dPid")Long [] dPid, @Param("startDate")Date startDate, @Param("endDate")Date endDate);
}
