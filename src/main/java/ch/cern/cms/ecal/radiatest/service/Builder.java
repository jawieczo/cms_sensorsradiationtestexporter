package ch.cern.cms.ecal.radiatest.service;

import java.util.List;

public interface Builder<T>
{
    String build(List<T> planeLines);
}
