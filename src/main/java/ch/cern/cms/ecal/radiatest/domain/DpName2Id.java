package ch.cern.cms.ecal.radiatest.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "DP_NAME2ID")
@Getter
public class DpName2Id
{
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "DPNAME")
    private String dpName;

    @Column(name = "UPDATE_COUNT")
    private Long updateCount;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "DPID")
    private List<RadSensorDpt> radSensorDpts;
}
