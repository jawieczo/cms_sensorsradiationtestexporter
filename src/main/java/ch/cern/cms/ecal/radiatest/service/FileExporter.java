package ch.cern.cms.ecal.radiatest.service;

import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;

public class FileExporter implements Exporter
{
    private String mFilename;

    public FileExporter(String aFilename)
    {
        mFilename = aFilename;
    }

    @Override
    public void export(byte[] aData) throws IOException
    {
        FileOutputStream fileOutputStream;

        fileOutputStream = new FileOutputStream(mFilename, true);
        fileOutputStream.write(aData);
        fileOutputStream.flush();
        fileOutputStream.close();
    }
}
