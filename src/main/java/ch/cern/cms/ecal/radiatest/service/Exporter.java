package ch.cern.cms.ecal.radiatest.service;

import java.io.IOException;

public interface Exporter
{
    void export(byte[] aData) throws IOException;
}
