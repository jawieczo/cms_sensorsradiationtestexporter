package ch.cern.cms.ecal.radiatest;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.application.Application;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Objects;

@SpringBootApplication
public class RadiatestApplication extends Application
{
    private ConfigurableApplicationContext context;
    private SpringApplicationBuilder builder;
    private Parent rootNode;

    public static void main(String[] args) {
        System.setProperty("oracle.net.tns_admin", System.getenv("TNS_ADMIN"));

        //SpringApplication.run(RadiatestApplication.class, args);
        Application.launch(RadiatestApplication.class, args);
    }

    @Override
    public void init() throws Exception
    {
        builder = new SpringApplicationBuilder(RadiatestApplication.class);
        context = builder.run(getParameters().getRaw().toArray(new String[0]));

        FXMLLoader loader = new FXMLLoader(RadiatestApplication.class.getClassLoader().getResource("main.fxml"));
        loader.setControllerFactory(context::getBean);
        rootNode = loader.load();
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        double width = 738.0;
        double height = 405.0;

        primaryStage.setScene( new Scene(rootNode, width, height));
        primaryStage.centerOnScreen();
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new Image(Objects.requireNonNull(RadiatestApplication.class.getClassLoader().getResourceAsStream("CMSlogo.png"))));
        primaryStage.setTitle("Sensors radiation test");
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception
    {
        context.close();
    }
}
