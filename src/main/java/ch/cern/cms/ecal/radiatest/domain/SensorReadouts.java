package ch.cern.cms.ecal.radiatest.domain;

import ch.cern.cms.ecal.radiatest.components.DateTimePicker;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.repository.Query;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

@NamedNativeQuery(name = "SensorReadoutsQuery",
        query = SensorReadouts.QUERY)
@Entity
@Getter
@Setter
public class SensorReadouts
{
    public static final String QUERY = "select updateid, DPNAME, change_date, value, rawvalue " +
            "from DP_NAME2ID inner join radsensordpt " +
            "on dp_name2id.id=radsensordpt.dpid " +
            "where DPNAME in (:dpnames) and radsensordpt.change_date \n" +
            "between :from " +
            "AND :to order by dpname, change_date desc";

    @Id
    @Column(name = "updateid")
    private Long updateId;

    @Column(name = "dpname")
    private String dpName;

    @Column(name = "change_date")
    private Date changeDate;

    @Column(name = "value")
    private BigDecimal value;

    @Column(name = "rawvalue")
    private Long rawValue;
}
