package ch.cern.cms.ecal.radiatest.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "radsensordpt")
@Getter
public class RadSensorDpt
{
    @Id
    @Column(name = "UPDATEID")
    private Long updateId;

    @Column(name = "DPID")
    private Long dPid;

    @Column(name = "CHANGE_DATE")
    private Date changeDate;

    @Column(name = "DPE_STATUS")
    private Long dpeStatus;

    @Column(name = "DPE_POSITION")
    private Long dpePosition;

    @Column(name = "VALUE")
    private BigDecimal value;

    @Column(name = "RAWVALUE")
    private Long rawValue;
}
