package ch.cern.cms.ecal.radiatest.service;

import ch.cern.cms.ecal.radiatest.components.DateTimePicker;
import ch.cern.cms.ecal.radiatest.domain.SensorReadouts;
import com.opencsv.CSVWriter;
import org.springframework.stereotype.Service;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.List;

@Service
public class CsvBuilder implements Builder<SensorReadouts>
{
    @Override
    public String build(List<SensorReadouts> planeLines)
    {
        String[] header = { "DP_NAME", "CHANGE_DATE", "VALUE", "RAW_VALUE"};

        StringWriter s = new StringWriter();
        CSVWriter writer = new CSVWriter(s);

        writer.writeNext(header);
        planeLines.forEach(sensorReadouts -> {
            SimpleDateFormat sDf = new SimpleDateFormat(DateTimePicker.DefaultFormat);
            String sDpName = sensorReadouts.getValue() != null ? sensorReadouts.getDpName() : "";
            String sChangeDate = sensorReadouts.getChangeDate() != null ? sDf.format(sensorReadouts.getChangeDate()) : "";
            String sValue = sensorReadouts.getValue() !=
                    null ? sensorReadouts.getValue().setScale(2, BigDecimal.ROUND_UP).toString() : "";
            String sRawValue = sensorReadouts.getRawValue() != null ? sensorReadouts.getRawValue().toString() : "";
            writer.writeNext(new String[]{sDpName, sChangeDate, sValue, sRawValue});
        });

        return s.toString();
    }
}
