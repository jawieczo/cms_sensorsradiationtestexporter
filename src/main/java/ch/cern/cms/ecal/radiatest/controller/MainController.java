package ch.cern.cms.ecal.radiatest.controller;

import ch.cern.cms.ecal.radiatest.components.DateTimePicker;
import ch.cern.cms.ecal.radiatest.domain.SensorReadouts;
import ch.cern.cms.ecal.radiatest.ds.DpeDao;
import ch.cern.cms.ecal.radiatest.service.CsvBuilder;
import ch.cern.cms.ecal.radiatest.service.FileExporter;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.value.ObservableBooleanValue;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class MainController
{
    @FXML
    public Button exportButton;

    @FXML
    public CheckBox flow1;

    @FXML
    public CheckBox flow2;

    @FXML
    public CheckBox vaisala1;

    @FXML
    public CheckBox vaisala2;

    @FXML
    public CheckBox wetMirrorDP;

    @FXML
    public CheckBox wetMirrorAT;

    @FXML
    public CheckBox pt1000;

    @FXML
    public CheckBox libPicco1RH;

    @FXML
    public CheckBox libPicco2RH;

    @FXML
    public CheckBox libPicco1DP;

    @FXML
    public CheckBox libPicco2DP;

    @FXML
    public CheckBox libPicco1DPerror;

    @FXML
    public CheckBox libPicco2DPerror;

    @FXML
    public DateTimePicker dateFrom;

    @FXML
    public TextField file;

    @FXML
    public DateTimePicker dateTo;

    @FXML
    public CheckBox wetMirrorDP2;

    @FXML
    public CheckBox wetMirrorAT2;

    @FXML
    public CheckBox libPicco3RH;

    @FXML
    public CheckBox libPicco3DP;

    @FXML
    public CheckBox libPicco3DPerror;

    @FXML
    public CheckBox wetMirrorRH1;
    
    @FXML
    public CheckBox wetMirrorRH2;
    
    private File selectedFile;

    private final DpeDao dpeDao;

    private final CsvBuilder csvBuilder;

    @Autowired
    public MainController(DpeDao dpeDao, CsvBuilder csvBuilder)
    {
        this.dpeDao = dpeDao;
        this.csvBuilder = csvBuilder;
    }

    @PersistenceContext
    private EntityManager entityManager;

    private final String AIR_TEMP_1 = "airTemp1";
    private final String DEW_POINT_VAISALA_1 = "dewPointVaisala1";
    private final String DEW_POINT_VAISALA_2 = "dewPointVaisala2";
    private final String DEW_POINT_WET_MIRROR_1 = "dewPointWetMirror1";
    private final String FLOW_1 = "flow1";
    private final String FLOW_2 = "flow2";
    private final String LIN_PICCO_DP_1 = "linPiccoDp1";
    private final String LIN_PICCO_DP_2 = "linPiccoDp2";
    private final String LIN_PICCO_DP_ERROR_1 = "linPiccoDpError1";
    private final String LIN_PICCO_DP_ERROR_2 = "linPiccoDpError2";
    private final String LIN_PICCO_RH_1 = "linPiccoRh1";
    private final String LIN_PICCO_RH_2 = "linPiccoRh2";
    private final String PT_1000 = "pt1000";
    private final String WET_MIRROR_DP_2 = "dewPointWetMirror2";
    private final String WET_MIRROR_AT_2 = "airTemp2";
    private final String LIB_PICCO_3RH = "linPiccoRh3";
    private final String LIB_PICCO_3DP = "linPiccoDp3";
    private final String LIB_PICCO_3D_PERROR = "linPiccoDpError3";
    private final String WET_MIRROR_RH_1 = "rhWetMirror1";
    private final String WET_MIRROR_RH_2 = "rhWetMirror2";
    
    private final String SYSTEM = "System1:";

    private List<String> checkSensors()
    {
        List<String> dpNames = new ArrayList<>();

        if(flow1.isSelected()) dpNames.add(SYSTEM + FLOW_1);
        if(flow2.isSelected()) dpNames.add(SYSTEM + FLOW_2);
        if(vaisala1.isSelected()) dpNames.add(SYSTEM + DEW_POINT_VAISALA_1);
        if(vaisala2.isSelected()) dpNames.add(SYSTEM + DEW_POINT_VAISALA_2);
        if(wetMirrorDP.isSelected()) dpNames.add(SYSTEM + DEW_POINT_WET_MIRROR_1);
        if(wetMirrorAT.isSelected()) dpNames.add(SYSTEM + AIR_TEMP_1);
        if(pt1000.isSelected()) dpNames.add(SYSTEM + PT_1000);
        if(libPicco1RH.isSelected()) dpNames.add(SYSTEM + LIN_PICCO_RH_1);
        if(libPicco2RH.isSelected()) dpNames.add(SYSTEM + LIN_PICCO_RH_2);
        if(libPicco1DP.isSelected()) dpNames.add(SYSTEM + LIN_PICCO_DP_1);
        if(libPicco2DP.isSelected()) dpNames.add(SYSTEM + LIN_PICCO_DP_2);
        if(libPicco1DPerror.isSelected()) dpNames.add(SYSTEM + LIN_PICCO_DP_ERROR_1);
        if(libPicco2DPerror.isSelected()) dpNames.add(SYSTEM + LIN_PICCO_DP_ERROR_2);
        if(wetMirrorDP2.isSelected()) dpNames.add(SYSTEM + WET_MIRROR_DP_2);
        if(wetMirrorAT2.isSelected()) dpNames.add(SYSTEM + WET_MIRROR_AT_2);
        if(libPicco3RH.isSelected()) dpNames.add(SYSTEM + LIB_PICCO_3RH);
        if(libPicco3DP.isSelected()) dpNames.add(SYSTEM + LIB_PICCO_3DP);
        if(libPicco3DPerror.isSelected()) dpNames.add(SYSTEM + LIB_PICCO_3D_PERROR);
        if(wetMirrorRH1.isSelected()) dpNames.add(SYSTEM + WET_MIRROR_RH_1);
        if(wetMirrorRH2.isSelected()) dpNames.add(SYSTEM + WET_MIRROR_RH_2);
        
        return dpNames;
    }

    @FXML
    public void initialize()
    {
        file.setOnMouseClicked(event -> {
            FileChooser fileChooser = new FileChooser();
            Node source = (Node) event.getSource();
            Window primaryStage = source.getScene().getWindow();

            selectedFile = fileChooser.showOpenDialog(primaryStage);

            if(selectedFile != null)
            {
                String path = selectedFile.getPath();
                file.setText(path);
            }
        });

        exportButton.setOnAction((event) -> {

            Date from = Date.from(dateFrom.getDateTimeValue().atZone(ZoneId.systemDefault()).toInstant());
            Date to = Date.from(dateTo.getDateTimeValue().atZone(ZoneId.systemDefault()).toInstant());
            List<String> sensors = checkSensors();

            Query query = entityManager.createNativeQuery(SensorReadouts.QUERY, SensorReadouts.class);
            query.setParameter("dpnames", sensors);
            query.setParameter("from", from);
            query.setParameter("to", to);

            List<SensorReadouts> sensorReadoutsList = query.getResultList();

            String csvString = csvBuilder.build(sensorReadoutsList);
            FileExporter exporter = selectedFile != null ? new FileExporter(selectedFile.getPath()):  new FileExporter("data.csv");

            try {
                exporter.export(csvString.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        // validation
        BooleanBinding booleanBind = file.textProperty().isEmpty().
                or(dateFrom.getEditor().textProperty().isEmpty()).
                or(dateTo.getEditor().textProperty().isEmpty()).
                or(Bindings.createBooleanBinding(this::validateDate));


        exportButton.disableProperty().bind(booleanBind);
    }

    private Boolean validateDate()
    {
        SimpleDateFormat sDf = new SimpleDateFormat(DateTimePicker.DefaultFormat);

        try
        {
            sDf.parse(dateTo.getEditor().getText());
            return true;
        } catch (ParseException ex)
        {
            exportButton.disableProperty().setValue(true);
            return false;
        }
    }
}
